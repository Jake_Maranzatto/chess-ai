from graphics import *
import numpy as np
import random
import math
import copy
import time
import functools
import collections as cl
from  itertools import islice

#hoping this works correctly
import Board

class AI:
    
    def __init__(self, board):
        self.board = board
        self.transpositions = cl.OrderedDict()
    #test method for functionality
    #returns a piece and where it moves to
    #unless you are in check, then it moves the king or a piece to block

            
    def randomMove(self, toMove, inCheck, kingPos):

        while True:
            if inCheck == False:
                randomPiece = random.choice(self.board.getPieces(toMove))
                moves = self.board.pieceMoves(randomPiece)
                
                if len(moves) > 0:
                    randomMove = random.choice(moves)
                    return (randomPiece, randomMove)
                else:
                    continue
                
            else:
                assert(kingPos != None)
                randomPiece = kingPos
                moves = self.board.pieceMoves(randomPiece)
                if len(moves) > 0:
                    randomMove = random.choice(moves)
                    return (randomPiece, randomMove)
                else:
                    while True:
                        randomPiece = random.choice(self.board.getPieces(toMove))
                        moves = self.board.pieceMoves(randomPiece)
                        if len(moves) > 0 and randomPiece[1] != 'K':
                            randomMove = random.choice(moves)
                            return (randomPiece, randomMove)
                        else:
                            continue

    def basicMaterialHueristic(self, position):
        pieceLocs = position.getPieces('W') + position.getPieces('B')
        pieces = []
        for i in pieceLocs:
            pos = position.board[i[0]][i[1]]
            pieces.append(pos)
        mapping = {
            'WP': 10,
            'BP': -10,
            'WR': 50,
            'BR': -50,
            'WN': 30,
            'BN': -30,
            'WB': 30,
            'BB': -30,
            'WK': 900,
            'BK': -900,
            'WQ': 90,
            'BQ': -90
            }
        return sum([mapping[i] for i in pieces])

    def returnCondition(self, position, toMove, other, depth):
        #if we win
        if depth <= 0:
            return self.basicMaterialHueristic(position)



    def alphaBeta(self, position, toMove, other, depth):
        moves = self.getMoves(position, toMove)
        bestMove = -9999
        bestMoveFound = None

        for mov in moves:
            position.push()
            position.move(*mov)
            value = self.miniMax(position, other, toMove, depth - 1, -10000, 10000)
            position.popSet()
            if value >= bestMove:
                bestMove = value
                bestMoveFound = mov
        print(bestMove)
        return bestMoveFound


    def miniMax(self, position, toMove, other, depth, alpha, beta):
        leaf = self.returnCondition(position, toMove, other, depth)
        if leaf != None:
            return -leaf
        #max
        if toMove == 'W':
            value = -50000
            moves = self.getMoves(position, toMove)
            #moves = self.getBestMoves(moves, 10)
            for mov in moves:
                position.push()
                position.move(*mov)
                if (position.boardToTuple()) in self.transpositions.keys():
                    value = max(value, self.transpositions[position.boardToTuple()])
                else:
                    value = max(value, self.miniMax(position, other, toMove, depth - 1, alpha, beta))
                    self.addToTransposition(position.boardToTuple(), toMove, value)
                position.popSet()
                alpha = max(alpha, value)
                if beta <= alpha:
                    return value
            return value
        #min
        else:
            value = 50000
            moves = self.getMoves(position, toMove)
            #moves = self.getBestMoves(moves, 10)
            for mov in moves:
                position.push()
                position.move(*mov)
                if (position.boardToTuple()) in self.transpositions.keys():
                    value = min(value, self.transpositions[position.boardToTuple()])
                else:
                    value = min(value, self.miniMax(position, other, toMove, depth - 1, alpha, beta))
                    self.addToTransposition(position.boardToTuple(), toMove, value)
                position.popSet()
                beta = min(beta, value)
                if beta <= alpha:
                    return value
            return value
                
    def playerToMove(self, player):
        return {'W':1, 'B':-1}[player]

    def getMoves(self, position, toMove):
        pieces = position.getPieces(toMove)
        moves = []
        for i in pieces:
            movs = (i, position.pieceMoves(i))
            for j in movs[1]:
                moves.append((movs[0], j))
        random.shuffle(moves) 
        for mov in moves:
            x = mov[1][0]
            y = mov[1][1]
            if position.board[x][y] != 0 or y == 0 or y == 7:
                moves.insert(0, moves.pop(moves.index(mov)))
        return moves
    
    def getBestMoves(self, moves, n):
        return sorted(moves, key = self.basicMaterialHueristic)[:n]

    def addToTransposition(self, boardState, toMove, evaluation):
        if len(self.transpositions.keys()) < 1000000:
            self.transpositions[boardState] =  evaluation
        else:
            while len(self.transpositions) > 500000:
                self.transpositions.popitem(last = True)
            self.transpositions[boardState] =  evaluation

    def move(self, toMove, inCheck, kingPos, depth):
        otherColor = ['W', 'B']
        otherColor.remove(toMove)
        otherColor = otherColor[0]
        k = self.alphaBeta(self.board, toMove, otherColor, depth)
        
        if k == None:
            print('rando')
            return self.randomMove(toMove, inCheck, kingPos)
        else:
            return k















