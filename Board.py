from graphics import *
import numpy as np
import math
import copy
import time
from numba import jit

class Visual:
    def __init__(self, Board):
        self.Board = Board
        self.size = 480
        self.win = GraphWin("Chess", self.size, self.size)
        
        self.textToImage = {
            0:0,
            'WP': './Pieces/Chess_plt60.png',
            'BP': './Pieces/Chess_pdt60.png',
            'WR':'./Pieces/Chess_rlt60.png',
            'BR': './Pieces/Chess_rdt60.png',
            'WN': './Pieces/Chess_nlt60.png',
            'BN': './Pieces/Chess_ndt60.png',
            'WB': './Pieces/Chess_blt60.png',
            'BB': './Pieces/Chess_bdt60.png',
            'WK': './Pieces/Chess_klt60.png',
            'BK': './Pieces/Chess_kdt60.png',
            'WQ': './Pieces/Chess_qlt60.png',
            'BQ': './Pieces/Chess_qdt60.png'
            }
        
        self.visualBoard = []
        for i in range(8):
            self.visualBoard.append([])
            for j in range(8):
                if self.textToImage[self.Board.board[i][j]] != 0:
                    xpos = i * self.size/8 + 29
                    ypos = j * self.size/8 + 31
                    position = Point(xpos, ypos)
                    self.visualBoard[i].append(Image(position, self.textToImage[self.Board.board[i][j]]))
                else:
                    self.visualBoard[i].append(0)

    def getWin(self):
        return self.win

    def reset(self):
        self.win.close()
        del self.win
        self.Board.reset()
        self.__init__(self.Board)
    
    def initVisualization(self):
         for i in range(8):
            for j in range(8):
                step = self.size/8
                xPos = i * step
                yPos = j * step
                if (i + j) % 2 == 1:
                    r = Rectangle(Point(xPos, yPos), Point(xPos + step, yPos + step))
                    r.draw(self.win)
                    r.setFill('grey')
                    r.setWidth(0)
                else:
                    r = Rectangle(Point(xPos, yPos), Point(xPos + step, yPos + step))
                    r.draw(self.win)
                    r.setFill('white')
                    r.setWidth(0)                
                    
    def initDisplay(self):
        for i in range(8):
            for j in range(8):
                if self.Board.board[i][j] != 0:
                    self.visualBoard[i][j].draw(self.win)
                    #Text(Point(yPos + step/2 , xPos + step/2), self.board[i][j]).draw(self.win)

    def draw(self):
        for i in range(8):
            for j in range(8):
                if self.Board.board[i][j] == self.Board.prevBoard[i][j]:
                    continue
                else:
                    if self.visualBoard[i][j] != 0:
                        self.visualBoard[i][j].undraw()
                    self.visualBoard[i][j] = self.textToImage[self.Board.board[i][j]]
                    if self.visualBoard[i][j] != 0:
                        xpos = i * self.size/8 + 29
                        ypos = j * self.size/8 + 31
                        position = Point(xpos, ypos)
                        self.visualBoard[i][j] = Image(position, self.visualBoard[i][j])
                        self.visualBoard[i][j].draw(self.win)
    
#more visualization methods
    
    def squaresForMoves(self, piece):
        sqrs = self.Board.pieceMoves(piece)
        vals = []
        for i in sqrs:
            xPos = i[0] * 60
            yPos = i[1] * 60

            xPosPlus = xPos + 60
            yPosPlus = yPos + 60
            vals.append(Line(Point(xPos, yPos), Point(xPos, yPosPlus)))
            vals.append(Line(Point(xPos, yPos), Point(xPosPlus, yPos)))
            vals.append(Line(Point(xPosPlus, yPosPlus), Point(xPos, yPosPlus)))
            vals.append(Line(Point(xPosPlus, yPosPlus), Point(xPosPlus, yPos)))
        return vals

    def drawMoves(self, vals):
        for r in vals:
            r.draw(self.win)
            r.setFill('red')
            r.setWidth(2.5)

    def undrawMoves(self, vals):
        for r in vals:
            r.undraw()                

############################################################################
        
class Board:
    def __init__(self, boardState = None):

        if boardState == None:
            #initializing empty board
            self.board = [[0 for i in range(8)] for j in range(8)]
            
            #initializing pawns
            for i in range(8):
                self.board[i][1] = 'BP'
                self.board[i][6] = 'WP'
            #initializing rooks
            self.board[0][0] = 'BR'
            self.board[7][0] = 'BR'
            self.board[0][7] = 'WR'
            self.board[7][7] = 'WR'
            #initializing bishops
            self.board[2][0] = 'BB'
            self.board[5][0] = 'BB'
            self.board[2][7] = 'WB'
            self.board[5][7] = 'WB'
            #initializing knights
            self.board[1][0] = 'BN'
            self.board[6][0] = 'BN'
            self.board[1][7] = 'WN'
            self.board[6][7] = 'WN'
            #initializing kings and queens
            self.board[3][7] = 'WQ'
            self.board[4][7] = 'WK'
            self.board[4][0] = 'BK'
            self.board[3][0] = 'BQ'

            self.prevBoard = self.board
            #previous board states for
            #Castling helper variables, with coordinates

            #black king
            self.castlingRights = {
                'BK': True,
                'WK': True,
                'R00': True,
                'R70': True,
                'R07': True,
                'R77': True
            }

            self.kingPos = {
                'WK': (4,7),
                'BK': (4,0)
            }
            
        else:
            self.board = boardState[0]
            self.castlingRights = boardState[1]
            self.kingPos = boardState[2]
            
        self.statesStack = []
        
    def boardToTuple(self):
        return tuple(tuple(i) for i in self.board)

    def pgnToBoard(self, form):
        #fill
        return None

    def boardToPgn(self):
        #fill
        return None
    
    def push(self):
        self.statesStack.append(([i[:] for i in self.board], copy.deepcopy(self.castlingRights), copy.deepcopy(self.kingPos)))

    def pop(self):
        return self.statesStack.pop()

    def popSet(self):
        x = self.statesStack.pop()
        self.board = x[0]
        self.castlingRights = x[1]
        self.kingPos = x[2]

    def reset(self):
        self.__init__()
        
    def setBoard(self, inBoard, inPrev):
        self.board = inBoard
        self.prevBoard = inPrev

    #SETTING UP PIECE DYNAMICS
    def pawnMove(self, x, y, dx, dy, myColor, otherColor, targetValue, tester):
##        for i in self.prevBoard:
##            print(i)
##        print()
##        for i in self.board:
##            print(i)
        forward = {'W':1, 'B':-1}[myColor]
        try:
            #checking en passant condition
            try:
                if int(targetValue) == 0:
                    if self.board[dx][y] == otherColor + 'P':
                        if self.prevBoard[dx][dy - forward] == otherColor + 'P':
                            if self.board[dx][dy - forward] == 0:
                                if self.prevBoard[dx][dy] == 0:
                                    return (True, 'P')
            except:
                dummy = None
                    
            #capture condition
            if targetValue[0] == otherColor:
                if forward*(y-dy) == 1 and abs(x-dx) == 1:
                    return True
                else:
                    return False
            #normal pawn move
            elif forward*(y-dy) == 1 and (x - dx) == 0:
                return True
            #double pawn advance from start squares
            elif forward*(y-dy) == 2 and (x - dx == 0) and self.board[x][y + -forward] == 0 and (y == 1 or y == 6):
                return True
        except Exception as e:
            print(e)
            return False

    def knightMove(self, x, y, dx, dy, myColor, otherColor, targetValue, tester):
        try:
            #we have eight conditions to deal with
            if (((x - dx) == 1 and abs(y - dy) == 2) or ((x - dx) == 2 and abs(y - dy) == 1)) or (((x - dx) == -1 and abs(y - dy) == 2) or ((x - dx) == -2 and abs(y - dy) == 1)):
                return True
            else:
                return False
        except:
            return False
        
    def rookMove(self, x, y, dx, dy, myColor, otherColor, targetValue, tester):
        if (x - dx) != 0 and (y-dy) != 0:
            return False
        
        if (x-dx) == 0:
            #no x movement
            yDir = int((dy - y)/abs(dy-y))
            xDir = 0
            posi = [x,y]
            for i in range(abs(y-dy)-1):
                test = self.board[posi[0]][posi[1]+yDir]
                if test != 0:
                    return False
                posi[1] += yDir
            if tester == False:
                self.castlingRights['R' + str(x) + str(y)] = False
            return True
                
        elif (y-dy) == 0:
            #no y movement
            xDir = int((dx-x)/abs(dx-x))
            yDir = 0
            posi = [x,y]
            for i in range(abs(x-dx)-1):
                test = self.board[posi[0] + xDir][posi[1]]
                if test != 0:
                    return False
                posi[0] += xDir
            if tester == False:
                self.castlingRights['R' + str(x) + str(y)] = False
            return True


    def bishopMove(self, x, y, dx, dy, myColor, otherColor, targetValue, tester):
        if abs(x-dx) != abs(y-dy):
            return False
        
        xDir = int((dx - x)/abs(dx - x))
        yDir = int((dy - y)/abs(dy - y))
        posi = [x, y]
        if abs(x-dx) == 1:
            return True
        for i in range(abs(x-dx)-1):
            test = self.board[posi[0]+xDir][posi[1]+yDir]
            if test != 0:
                return False
            posi[0] += xDir
            posi[1] += yDir
        return True
    
    def kingMove(self, x, y, dx, dy, myColor, otherColor, targetValue, tester):
        if (x == dx) and (y == dy):
            return False
        
        if targetValue != myColor + 'R':
            if targetValue[0] == myColor:
                return False
            
            if math.sqrt((x-dx)**2 + (y-dy)**2) < 2:
                if (self.bishopMove(x, y, dx, dy, myColor, otherColor, targetValue, tester) == True) or (self.rookMove(x, y, dx, dy, myColor, otherColor, targetValue, tester) == True):
                    #if self.squareControlCheck(otherColor, myColor, (dx,dy)) == False:
                        if tester == False:
                            self.castlingRights[myColor + 'K'] = False
                            self.kingPos[myColor + 'K'] = (dx, dy)
                        return True
                return False
            return False
        
        #checking for castling rights for king and rook
        elif targetValue == myColor + 'R' and self.castlingRights[myColor + 'K']:
            if (dx == 0 or dx == 7) and (dy == 0 or dy == 7) and self.castlingRights['R' + str(dx) + str(dy)]:
                #opponentControlledSquares = self.squareControlCheck(myColor, otherColor)
                direction = int((dx - x)/abs(dx - x))
                posi = [x,y]
                for i in range(abs(x-dx) - 1):
                    test = self.board[posi[0]+direction][posi[1]]
                    #self.squareControlCheck(player, otherPlayer, squareToCheck, pieceSquares = None)
                    controlTest = self.squareControlCheck(otherColor, myColor, (posi[0] + direction, posi[1]))[0]
                    if test != 0 or controlTest:
                        return False
                    posi[0] += direction 
                return (True, 'K')
            
            return False
        return False
                
    #going to be a hefty function..
                
    def legalMove(self, position, moveTo, tester = False):
        x = position[0]
        y = position[1]
        dx = moveTo[0]
        dy = moveTo[1]
        #print(self.board[x][y], self.board[dx][dy])
        
        #if we're off of the board
        if dx < 0 or dx > 7 or dy < 0 or dy > 7:
            return False
        
        if self.board[x][y] == 0:
            return False
        #getting color
        myColor = self.board[x][y][0]
        myPiece = self.board[x][y][1]
        otherColor = ['W', 'B']
        otherColor.remove(myColor)
        otherColor = otherColor[0]
        
        targetValue = self.board[dx][dy]
        if type(targetValue) != str:
            targetValue = str(targetValue)
            
        if targetValue == '0' or targetValue[0] == otherColor or myPiece =='K':
            #legalPawnMove
            if myPiece == 'P':
                return self.pawnMove(x, y, dx, dy, myColor, otherColor, targetValue, tester)
            #legalRookMove
            if myPiece == 'R':
                return self.rookMove(x, y, dx, dy, myColor, otherColor, targetValue, tester)    
            #legalKnightMove
            if myPiece == 'N':
                return self.knightMove(x, y, dx, dy, myColor, otherColor, targetValue, tester)
            #legalBishopMove
            if myPiece == 'B':
                return self.bishopMove(x, y, dx, dy, myColor, otherColor, targetValue, tester)
            #legalQueenMove, can be done with a combination of rook and bishop moves
            if myPiece == 'Q':
                return (self.bishopMove(x, y, dx, dy, myColor, otherColor, targetValue, tester) or
                        self.rookMove(x, y, dx, dy, myColor, otherColor, targetValue, tester))
            #legalKingMove
            if myPiece == 'K':
                return self.kingMove(x, y, dx, dy, myColor, otherColor, targetValue, tester)
        
        return False

    def move(self, position, moveTo):
        #self.push()
        x = position[0]
        y = position[1]
        dx = moveTo[0]
        dy = moveTo[1]
        piece = self.board[x][y]
        #if we have an actual piece to move
        if piece != 0:
            
##            toX =  dx * self.size/8 + 29
##            toY =  dy * self.size/8 + 31
##            visualPiece = Image(Point(toX, toY), self.textToImage[self.board[x][y]])
            #and the piece wants a legal move
            mov = self.legalMove(position, moveTo)
            if type(mov) == tuple:
                
                if mov == (True, 'P'):
                    if self.promote(position, moveTo, 'Q'):
                        return True
                    else:
                        self.board[dx][y] = 0

                    
                elif mov == (True, 'K'):
                    myColor = self.board[x][y][0]
                    direction = int((dx - x)/abs(dx - x))
                    self.board[x+direction][y] = myColor + 'R'
                    self.board[x + 2* direction][y] = myColor + 'K'
                    self.board[x][y] = 0
                    self.board[dx][dy] = 0
                    self.castlingRights[myColor + 'K'] = False
                    self.kingPos[myColor + 'K'] = (x + 2 * direction, y)
                    return True
                
            if mov == True or (type(mov) == tuple and mov[0] == True):
                self.board[x][y] = 0
                self.board[dx][dy] = piece
            else:
                print('err')
                
        #giving return values for our AI to work with
                return False
        return True
    
    def promote(self, position, moveTo, promotePiece):
        x = position[0]
        y = position[1]
        dx = moveTo[0]
        dy = moveTo[1]
        piece = self.board[x][y]
        if piece == 0:
            return False
        if not (piece[0] == promotePiece[0] and (promotePiece[1] != 'P' or promotePiece[1] != 'K')):
            return False
        #make sure our piece is a pawn
        if piece[1] != 'P':
            return False
        
        if self.legalMove(position, moveTo, tester = True) and abs(y-dy) == 1:
            self.board[dx][dy] = promotePiece
            self.board[x][y] = 0
            return True
            
        else:
            return False

    #helper function for below
    def pieceControlCheck(self, player, otherPlayer, piece, squareToCheck):
        pieceX = piece[0]
        pieceY = piece[1]
        p = (self.board[pieceX][pieceY])
        assert(p[0] == player)
        
        if p[1] == 'P':
            forward = {'W':1, 'B':-1}[player]
            yCheck = (squareToCheck[1] == pieceY - forward)
            xCheck = (squareToCheck[0] == pieceX + 1 or squareToCheck[0] == pieceX -1)
            return (yCheck and xCheck)

        elif p[1] == 'K':
            x = piece[0]
            y = piece[1]
            squares = []
            for i in range(3):
                #above
                #if self.legalMove((x,y), (x-1, y+i - 1)):
                    squares.append((x-1, y+i - 1))
                #middle
                #if self.legalMove((x,y), (x, y+i - 1)):
                    squares.append((x, y+i - 1))
                #below
                #if self.legalMove((x,y), (x+1, y+i - 1)):
                    squares.append((x+1, y+i - 1))
            if squareToCheck in squares:
                return True
            else:
                return False
        
        else:
            return self.legalMove(piece, squareToCheck, True)



    def getPieces(self, player):
        pieceSquares = []
            
        for i in range(8):
            for j in range(8):
                if (self.board[i][j]) != 0:
                    if (self.board[i][j])[0] == player:
                        pieceSquares.append((i,j))
                            
        return pieceSquares
        
    #returns true if you control this square, even if the control
    #is very weak (eg. sac a queen for a pawn with no compensation)
    #has a parameter to break out of the routine if we find the king is in check
    #At this point used for check and checkmate analysis in positions
    def squareControlCheck(self, player, otherPlayer, squareToCheck, pieceSquares = None):
        if pieceSquares == None:
            pieceSquares = self.getPieces(player)
        ret = [False, []]
        for piece in pieceSquares:
            if self.pieceControlCheck(player, otherPlayer, piece, squareToCheck) == True:
                ret[0] = True
                ret[1].append(piece)
            
        return ret

    #sees if the player king is in check
    def check(self, player, otherPlayer):
        kingPos = self.kingPos[player + 'K']
        return self.squareControlCheck(otherPlayer, player, kingPos)[0]

    def squaresAroundKing(self, kingPosition):
        #kingPosition = self.board[kingPosition[0]][kingPosition[1]]
        #assert()
        x = kingPosition[0]
        y = kingPosition[1]
        squares = []
        for i in range(3):
            #above
            if self.legalMove((x,y), (x-1, y+i - 1)):
                squares.append((x-1, y+i - 1))
            #middle
            if self.legalMove((x,y), (x, y+i - 1)):
                squares.append((x, y+i - 1))
            #below
            if self.legalMove((x,y), (x+1, y+i - 1)):
                squares.append((x+1, y+i - 1))
        
        return (len(squares) > 0, squares)

    #returns true if color is to move, and color checks othercolor's king
    def winLose(self, color, otherColor):
        kingPos = self.kingPos[otherColor + 'K']
        #if the other king is in check

        controller = self.squareControlCheck(color, otherColor, kingPos)
        if controller[0]:
            return True
            #if the other player can move to take the king, return True
        # if toMove == otherColor:
        #     return True
        #     #and the king has no free squares
        #     if self.squaresAroundKing(kingPos)[0]:
        #         #and multicheck, return True
        #         if len(controller[1] > 1):
        #             return True
        #         #if not, and we can take the piece return false
        #         takePossibility = self.squareControlCheck(color, otherColor, controller[1])
        #         elif takePossibility[1]):
        #             self.push()
        #             self.move(takePossibility[1], controller[1])
        #             return self.check(color, otherColor)
        # return False
        #else:
            #see if we have a blocking condition
                    
    def pieceMoves(self, piece):
        moves = []
        for i in range(8):
            for j in range(8):
                mov = self.legalMove(piece, (i,j), True)
                if mov  == True or (type(mov) == tuple and mov[0] == True):
                    moves.append((i,j))
        return moves

    def newBoard(self):
        return copy.deepcopy(self)
#END CLASS###













