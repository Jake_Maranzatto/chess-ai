from graphics import *
import numpy as np
import math
import copy
import time
from PIL import Image

#hoping this works correctly
import Board
import AI
import os, glob

def humanPlayer(k, viz, toMove):
    piece = viz.getWin().getMouse()
    position = (int(piece.getX()/60), int(piece.getY()/60))
    piece = k.board[position[0]][position[1]]
    
    if type(piece) == int:
        return (0,0,0)
    
    if piece[0] == toMove:
        sqrs = viz.squaresForMoves(position)
        viz.drawMoves(sqrs)

    else:
        return (0,0,0)
    
    where = viz.getWin().getMouse()
    moveTo =  (int(where.getX()/60), int(where.getY()/60))
    
    viz.undrawMoves(sqrs)
    
    return (position, moveTo, piece)

def aiPlayer(k, viz, ai, currentPlayer, passKing, depth):
    position, moveTo = ai.move(currentPlayer, passKing, k.kingPos[currentPlayer + 'K'], depth)
    #print(position, moveTo)
    piece = k.board[position[0]][position[1]]
    
##    sqrs = k.squaresForMoves(position)
##    k.drawMoves(sqrs)
##    #time.sleep(1)
##    k.undrawMoves(sqrs)
    
    return (position, moveTo, piece)


k = Board.Board()
ai = AI.AI(k)
viz = Board.Visual(k)
##BEGIN MAIN##
def main(p1, p2, depth):
    viz.reset()
    viz.initVisualization()
    viz.initDisplay()
    
    current = 0
    players = ['W', 'B']
    
    winner = {
        'W':1,
        'B':-1
    }
    iteration = -1
    while True:
        iteration += 1
        print(iteration)
        viz.draw()
        # viz.win.postscript(file = './Games/Ply_' + str(iteration) + '.eps')
        print(players[current] + ' to play.')
        #drawBoard(k)
        #drawPrevBoard(k)

        #getting info on check situation
        passKing = (False, None)
        if k.check(players[current], players[::-1][current]):
            print(players[current] + ' is in check!')
            passKing = (True, k.kingPos[players[current] + 'K'])
    
        tempBoard = copy.deepcopy(k.board)

        if p1 == 'ai' and players[current] == 'W':
            position, moveTo, piece = aiPlayer(k, viz, ai, players[current], passKing, depth)
            
        elif p1 == 'player' and players[current] == 'W':
            position, moveTo, piece = humanPlayer(k, viz, players[current])

        elif p2 == 'ai' and players[current] == 'B':
            position, moveTo, piece = aiPlayer(k, viz, ai, players[current], passKing, depth)
            
        elif p2 == 'player' and players[current] == 'B':
            position, moveTo, piece = humanPlayer(k, viz, players[current])
        else:
            print('Not a valid player set.  Try again.')
            return 0

        
        if piece == 0 or piece[0] != players[current]:
            continue
        #print(piece)
        #incase we have a promotion
        #check rank, player, and piece
        if ((moveTo[1] == 0 and players[current] == 'W') or (moveTo[1] == 7 and players[current] == 'B')) and piece[1] == 'P' and abs(position[1]-moveTo[1]) == 1:

            #promo = input('Enter what you would like to promote to: ')
            promo = 'Q'
            toPromote = players[current] + promo
            k.promote(position, moveTo, toPromote)
            if k.winLose(players[::-1][current], players[current]):
                print(players[::-1][current] + ' wins!')
                return winner[players[::-1][current]]
            k.prevBoard = tempBoard
            players = players[::-1]
        
        elif (k.move(position, moveTo)):
            if k.winLose(players[::-1][current], players[current]):
                print(players[::-1][current] + ' wins!')
                return winner[players[::-1][current]]
            k.prevBoard = tempBoard
            players = players[::-1]

    #viz.undrawMoves()




t = main('player', 'ai', 4)
# os.chdir("./Games")
# lst = glob.glob('*.ps')
# for i, f in enumerate(lst):
#     outf = str(i) + ".jpg"
#     try:
#         Image.open(f).save(outf)
#     except Exception as e:
#         print(e)





        
